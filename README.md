CMU CUPS Password Meter
=======================

The CMU CUPS Password Meter is an open-source implementation of a password meter widget built based on the research of Carnegie Mellon University's [Cylab Usable Privacy and Security Laboratory](http://cups.cs.cmu.edu).

Features
--------

The CMU CUPS Password Meter is designed to be pluggable and extensible so that any particular component may be swapped out for a different variant. The components come together to form a chain of modules that take a test password as input, produce a strength score, and provide an informative display to the user regarding the strength of his password.

Appearance Styles
-----------------

### Baseline

Medium thickness meter appears below password field. Background is a light gray to indicate area of meter. Color shifts from red to green past threshold value.

### Thin

Thin meter appears below password field. Background is a light gray to indicate area of meter. Color shifts from red to green past threshold value.

### Thick

Thick meter appears below password field. Background is a light gray to indicate area of meter. Color shifts from red to green past threshold value.

### Bordered

Medium thickness meter appears below password field. Area of meter is outlined with a thin border. Color shifts from red to green past threshold value.

### Embedded

Medium thickness, short meter appears embedded in the password field. Background is a light gray to indicate area of meter. Color shifts from red to green past threshold value.

### Segmented meter

Medium thickness meter appears below password field. Background is a light gray to indicate area of meter. Meter is divided into equal width segmenets. Segments fill in sequence to indicate strength.

Example
-------

### HTML

    <form>
        <div id="password-field">
            <label for="password">Password</label>
            <input id="password" type="password" name="password">
        </div>
    </form>

### JavaScript

    $('#password').passwordMeter()


Development
-----------

To build this project, you must install [node.js](http://nodejs.org/). Then, execute the following commands:

    # Install grunt command line
    npm install -g grunt-cli bower

    # Within the project directory...
    npm install
    bower install

    # Run the `dist` task to build the distributable
    grunt dist

    # List all possible tasks
    grunt --help


Testing
-------

### Issues

* To run the `qunit` tests in Google Chrome, Chrome must be started with the `--allow-file-access-from-files` flag.

> ### Change local files security policy
>
> #### Safari
>
> Enable the develop menu using the preferences panel, under Advanced -> "Show develop menu in menu bar"
>
> Then from the safari "Develop" menu, select "Disable local file restrictions", it is also worth noting safari has some odd behaviour with caches, so it is advisable to use the "Disable caches" option in the same menu; if you are editing & debugging using safari.
>
> #### Chrome
> Close all running chrome instances first. Then start Chrome executable with a command line flag:
>
> ```
> chrome --allow-file-access-from-files
> ```
>
> On Windows, the easiest is probably to create a special shortcut which has added flag (right-click on shortcut -> properties -> target).
>
> #### Firefox
>
> 1. Go to `about:config`
> 2. Find `security.fileuri.strict_origin_policy` parameter
> 3. Set it to `false`

From https://github.com/mrdoob/three.js/wiki/How-to-run-things-locally


### Cross-browser testing resources

* Microsoft Internet Explorer (IE 9+)
    * [Modern.IE](http://modern.ie/)
* Mozilla Firefox
    * [Mozilla Developer Network](https://developer.mozilla.org)
* Google Chrome
    * [Google Developers -- Chrome](https://developers.google.com/chrome/)
* Apple Safari (Safari 6+)
    * [Safari for Developers](https://developer.apple.com/safari/)
