module.exports = (grunt) ->
  grunt.initConfig
    pkg: grunt.file.readJSON('package.json')


    coffee:
      src:
        options:
          join: true
        files:
          'src/passwordmeter.js': ['src/passwordmeter.coffee',
                                   'src/rules/*.coffee',
                                   'src/visualizations/*.coffee']
      dist:
        options:
          join: true
        files:
          'dist/passwordmeter.js': ['src/passwordmeter.coffee',
                                    'src/rules/*.coffee',
                                    'src/visualizations/*.coffee']
      test:
        expand: true
        cwd: 'test/'
        src: '**/*.coffee'
        dest: 'test/'
        ext: '.js'

    less:
      src:
        files:
          'src/passwordmeter.css': 'src/passwordmeter.less'
      dist:
        files:
          'dist/passwordmeter.css': 'src/passwordmeter.less'

    coffeelint:
      options:
        max_line_length:
          level: 'warn'
      gruntfile:
        options:
          max_line_length:
            level: 'ignore'
        src: 'Gruntfile.coffee'
      src: 'src/**/*.coffee'
      test: 'test/**/*.coffee'

    htmllint:
      options:
        ignore: [
          'Bad value “X-UA-Compatible” for attribute “http-equiv” on XHTML element “meta”.'
        ]
      demos: 'demos/**/*.html'
      test: 'test/**/*.html'

    lesslint:
      src: 'src/**/*.less'

    uglify:
      dist:
        files:
          'dist/passwordmeter.min.js': 'dist/passwordmeter.js'

    qunit:
      all: 'test/**/*.html'

    watch:
      gruntfile:
        files: '<%= coffeelint.gruntfile %>'
        tasks: 'coffeelint:gruntfile'
      src:
        options:
          livereload: true
        files: ['<%= coffeelint.src %>', '<%= lesslint.src %>']
        tasks: ['coffeelint:src', 'coffee:src',
                'lesslint:src', 'less:src', 'qunit']
      test:
        options:
          livereload: true
        files: ['<%= coffeelint.test %>', '<%= htmllint.test %>']
        tasks: ['coffeelint:test', 'htmllint:test', 'coffee:test', 'qunit']
      demos:
        options:
          livereload: true
        files: ['<%= htmllint.demos %>']
        tasks: ['htmllint:demos']


  grunt.loadNpmTasks 'grunt-coffeelint'
  grunt.loadNpmTasks 'grunt-contrib-coffee'
  grunt.loadNpmTasks 'grunt-contrib-less'
  grunt.loadNpmTasks 'grunt-contrib-qunit'
  grunt.loadNpmTasks 'grunt-contrib-uglify'
  grunt.loadNpmTasks 'grunt-contrib-watch'
  grunt.loadNpmTasks 'grunt-html'
  grunt.loadNpmTasks 'grunt-lesslint'
  grunt.loadNpmTasks 'grunt-notify'


  grunt.registerTask 'lint', [ 'coffeelint', 'htmllint', 'lesslint' ]
  grunt.registerTask 'compile', [ 'coffee:src', 'less:src' ]
  grunt.registerTask 'test', [ 'coffee:test', 'qunit' ]
  grunt.registerTask 'dist', [ 'lint', 'coffee:dist', 'less:dist', 'uglify' ]
  grunt.registerTask 'default', [ 'lint', 'compile', 'test' ]
