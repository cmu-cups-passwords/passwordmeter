###!
# Basic Visualization
# Author: Adam Durity <adurity@cmu.edu>
###

(($) ->

  class Basic
    options:
      basic: null

    constructor: (passwordInput, passwordMeter,
                  cssClass='cups-passwordmeter-basic') ->
      @passwordInput = passwordInput
      @passwordMeter = passwordMeter

      @passwordInput.addClass cssClass+"-input"

      @passwordMeter.addClass cssClass
      @passwordMeter.height()
      @span = $('<span></span>')
                .width(0)
                .appendTo(@passwordMeter)

      @_positionMeter()

    destroy: $.noop

    refresh: (value) ->
      newWidth = Math.min((value * @passwordMeter.width()) / 100.0,
                          @passwordMeter.width())
      @span.width(newWidth)

    _positionMeter: ->
      @passwordInput.css 'padding-bottom', "+=#{@passwordMeter.height()}"
      inputPadding =
        bottom: parseFloat(@passwordInput.css 'padding-bottom')
        left:   parseFloat(@passwordInput.css 'padding-left')
        right:  parseFloat(@passwordInput.css 'padding-right')

      inputBorder =
        bottom: parseFloat(@passwordInput.css 'border-bottom-width')
        left:   parseFloat(@passwordInput.css 'border-left-width')
        right:  parseFloat(@passwordInput.css 'border-right-width')

      inputMargin =
        bottom: parseFloat(@passwordInput.css 'margin-bottom')
        left: parseFloat(@passwordInput.css 'margin-left')

      top = @passwordMeter.height() - inputMargin.bottom -
        inputBorder.bottom - inputPadding.bottom
      @passwordMeter.css 'top', top

      left = inputMargin.left + inputBorder.left
      @passwordMeter.css 'left', left

      @passwordMeter.width(@passwordInput.width() +
                           inputPadding.left + inputPadding.right)

      @passwordMeter.css 'border-bottom-left-radius',
        @passwordInput.css('border-bottom-left-radius')
      @passwordMeter.css 'border-bottom-right-radius',
        @passwordInput.css('border-bottom-right-radius')


  $.extend $.cups.passwordmeter.visualizations,
    basic: Basic

) jQuery
