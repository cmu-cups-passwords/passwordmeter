###!
# CMU CUPS Password Meter
# Version: 0.1.0
###

(($) ->

  Function::property = (prop, desc) ->
    Object.defineProperty @prototype, prop, desc

  class PasswordScore
    _scale = 1
    _value = 0

    constructor: (@value=0, @scale=1)->
      @reset()

    @property 'scale',
      get: -> @_scale
      set: (v) -> @_scale *= @_minimumBound v, 0

    @property 'value',
      get: -> @_value
      set: (v) -> @_value = @_bound v

    reset: ->
      @_scale = 1
      @_value = 0

    score: ->
      @_bound @_scale * @_value

    _bound: (value) ->
      return @_maximumBound @_minimumBound(value)

    _minimumBound: (value, minBound=0) ->
      return Math.max value, minBound

    _maximumBound: (value, maxBound=100) ->
      return Math.min value, maxBound

  $.widget 'cups.passwordmeter',
    version: "0.1.0"
    options:
      rules: [
        'blacklist'
        'characterClass'
        'length'
        'minLength'
      ]
      visualization: 'basic'

    _create: ->
      @element.addClass 'cups-passwordmeter-input'
      @meter =
        $('<div>')
          .addClass 'cups-passwordmeter'
          .insertAfter @element

      this._createRules()
      this._initVisualization()

      this._on @element,
        'input': (event) ->
          this._updateScore @element.val()

      this.refresh()

    _createRules: ->
      rules = $.cups.passwordmeter.rules
      @_rules = (new rules[rule](@options[rule]) for rule in @options.rules)
      @_score = new PasswordScore()

    _destroy: ->
      @element.removeClass 'cups-passwordmeter-input'
      @visualization.destroy()

    _initVisualization: ->
      visualizations = $.cups.passwordmeter.visualizations
      if (typeof @options.visualization == 'string')
        @visualization =
          new visualizations[@options.visualization](@element, @meter)

    refresh: ->
      @visualization.refresh(@_score.score())

    score: ->
      @_score

    _updateScore: (password) ->
      @_score.reset()
      for rule in @_rules
        @_score = rule.grade(password, @_score)
      this.refresh()

  $.extend $.cups.passwordmeter,
    PasswordScore: PasswordScore
    rules: {}
    visualizations: {}

  return $.cups.passwordmeter

) jQuery
