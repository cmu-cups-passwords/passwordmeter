###
# CMU CUPS Password Meter - CharacterClass rule
###

(($) ->

  class CharacterClass
    LOWERCASE_LETTERS: new RegExp "[a-z]"
    UPPERCASE_LETTERS: new RegExp "[A-Z]"
    DIGITS: new RegExp "[0-9]"
    SYMBOLS: new RegExp "[#{['!','"','#','$','%','&','\'','(',')','*','+',',',
      '-','.','/',':',';','<','=','>','?','@','[','\\','\]','^','_','`','{','|'
      ,'}','~'].join('')}]"

    options:
      characterClass: null

    constructor: (options) ->
      @options = $.extend {}, @options, options

    grade: (password, score) ->
      numCharacterClasses = @_countCharacterClasses password
      if numCharacterClasses > 0
        score.scale = (-1 / numCharacterClasses) + 2
      return score

    _countCharacterClasses: (str) ->
      characterClasses = 0
      if str.match @LOWERCASE_LETTERS
        characterClasses++
      if str.match @UPPERCASE_LETTERS
        characterClasses++
      if str.match @DIGITS
        characterClasses++
      if str.match @SYMBOLS
        characterClasses++
      return characterClasses

  $.extend $.cups.passwordmeter.rules,
    characterClass: CharacterClass

) jQuery
