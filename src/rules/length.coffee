###
# CMU CUPS Password Meter - Length rule
###

(($) ->

  class Length
    options:
      length: 8

    constructor: (options) ->
      @options = $.extend {}, @options, options

    grade: (password, score) ->
      length = @_stripRepeatingCharacters(password).length
      lengthScore = (-99 / length**(2/@options.length)) + 100
      score.value = score.value + lengthScore
      return score

    _stripRepeatingCharacters: (str) ->
      return str.replace /(\w)(\1){2,}/g, "$1$1"


  $.extend $.cups.passwordmeter.rules,
    length: Length

) jQuery
