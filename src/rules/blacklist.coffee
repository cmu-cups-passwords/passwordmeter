###
# CMU CUPS Password Meter - Blacklist rule
###

(($) ->

  class Blacklist
    options:
      blacklist: [
        'password',
        'correcthorsebatterystaple'
      ]

    constructor: (options) ->
      @options = $.extend {}, @options, options

    grade: (password, score) ->
      if $.inArray(password, @options.blacklist) != -1
        score.scale = 0
      return score

  $.extend $.cups.passwordmeter.rules,
    blacklist: Blacklist

) jQuery
