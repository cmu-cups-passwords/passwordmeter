###
# CMU CUPS Password Meter - MinLength rule
###

(($) ->

  class MinLength
    options:
      minLength: 8

    constructor: (options) ->
      @options = $.extend {}, @options, options

    grade: (password, score) ->
      if password.length < @options.minLength
        minLengthScore = (password.length * 40) // @options.minLength
        score.value = Math.min score.value, minLengthScore
      return score

  $.extend $.cups.passwordmeter.rules,
    minLength: MinLength

) jQuery
