###
# CMU CUPS Password Meter - MinLength rule
###

(($) ->

  class ChallengeFactor
    options:
      challengeFactor: 2.0

    constructor: (options) ->
      @options = $.extend {}, @options, options

    grade: (password, score) ->
      score.scale = 1 / @options.challengeFactor
      return score

  $.extend $.cups.passwordmeter.rules,
    challengeFactor: ChallengeFactor

) jQuery
