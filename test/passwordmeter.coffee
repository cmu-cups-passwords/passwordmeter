(($) ->

  widget = 'passwordmeter'
  widgetDefaults = $.cups[widget].prototype.options
  defaults =
    disabled: false
    rules: [
      'blacklist'
      'characterClass'
      'length'
      'minLength'
    ]
    visualization: 'basic'

    # callbacks
    create: null


  module widget

  test 'basic usage', ->
    expect 3

    defaultElement = $.cups[widget].prototype.defaultElement
    $(defaultElement).appendTo('body')[widget]().remove()
    ok true, 'initialized on element'

    $(defaultElement)[widget]().remove()
    ok true, 'initialized on disconnected DOMElement - never connected'

    $(defaultElement).appendTo('body').remove()[widget]().remove()
    ok true, 'initialized on disconnected DOMElement - removed'

  test 'defined defaults', ->
    count = 0
    $.each defaults, (key, val) ->
      expect ++count
      if $.isFunction(val)
        ok $.isFunction(widgetDefaults[key]), key
      else
        deepEqual widgetDefaults[key], val, key

  test 'tested defaults', ->
    count = 0
    $.each widgetDefaults, (key) ->
      expect ++count
      ok key of defaults, key

  test 'version', ->
    expect 1
    ok 'version' of $.cups[widget].prototype, 'version property exists'

  test 'markup structure', ->
    expect 3

    element = $('input[type=password]').passwordmeter()
    ok element.hasClass('cups-passwordmeter-input'),
      'input element is .cups-passwordmeter-input'

    meter = element.nextAll()
    equal meter.length, 1, 'one password meter inserted after element'
    ok meter.is('div.cups-passwordmeter'),
      'password meter is div.cups-passwordmeter'

  # test 'destroy', ->
  #   expect 1
  #   domEqual 'input[type=password]', ->
  #     $('input[type=password]').passwordmeter().passwordmeter('destroy')

  # test 'score', ->
  #   expect 1
  #   element = $('input[type=password]').passwordmeter()
  #   equal element.passwordmeter('score'), new PasswordScore(), 'score == 0'

) jQuery
