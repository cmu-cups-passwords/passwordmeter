(($) ->

  rule = 'length'
  widget = 'passwordmeter'

  module "#{widget}: #{rule}"

  test 'exists', ->
    expect 1
    ok rule of $.cups.passwordmeter.rules, 'rule extends widget'

  defaults =
    length: 8
  ruleDefaults = $.cups.passwordmeter.rules[rule].prototype.options

  test 'defined defaults', ->
    count = 0
    $.each defaults, (key, val) ->
      expect ++count
      if $.isFunction(val)
        ok $.isFunction(ruleDefaults[key]), key
      else
        deepEqual ruleDefaults[key], val, key

  test 'tested defaults', ->
    count = 0
    $.each ruleDefaults, (key) ->
      expect ++count
      ok key of defaults, key

  test 'stripRepeatingCharacters', ->
    expect 4
    instance = new $.cups.passwordmeter.rules[rule]()

    equal instance._stripRepeatingCharacters('cat'), 'cat',
      'does not change string with no repeating characters'
    equal instance._stripRepeatingCharacters('catty'), 'catty',
      'does not change string with double characters'
    equal instance._stripRepeatingCharacters('caaaat'), 'caat',
      'strips single group of repeating characters down to double'
    equal instance._stripRepeatingCharacters('caaatinthehaaat'),
      'caatinthehaat', 'strips multiple groups of repeating characters'

) jQuery
