(($) ->

  rule = 'characterClass'
  widget = 'passwordmeter'
  instance = null

  module "#{widget}: #{rule}"

  test 'exists', ->
    expect 1
    ok rule of $.cups.passwordmeter.rules, 'rule extends widget'

  defaults =
    characterClass: null
  ruleDefaults = $.cups.passwordmeter.rules[rule].prototype.options

  test 'defined defaults', ->
    count = 0
    $.each defaults, (key, val) ->
      expect ++count
      if $.isFunction(val)
        ok $.isFunction(ruleDefaults[key]), key
      else
        deepEqual ruleDefaults[key], val, key

  test 'tested defaults', ->
    count = 0
    $.each ruleDefaults, (key) ->
      expect ++count
      ok key of defaults, key

  module "#{rule}",
    setup: ->
      instance = new $.cups.passwordmeter.rules[rule]()

  test "#{rule}:_countCharacterClasses", ->
    passwords =
      '': 0
      'password': 1
      'Password': 2
      'Passw0rd': 3
      'Passw0rd!': 4

    expect Object.keys(passwords).length

    $.each passwords, (password, expected) ->
      equal instance._countCharacterClasses(password), expected,
        "#{password} should have #{expected} character classes"

  test "#{rule}:grade", ->
    passwords =
      '': 1
      'password': 1
      'Password': 1.5
      'Passw0rd': (5/3)
      'Passw0rd!': 1.75

    expect (2 * Object.keys(passwords).length)

    $.each passwords, (password, expected_scale) ->
      score = instance.grade password, new $.cups.passwordmeter.PasswordScore()
      equal score.scale, expected_scale,
        "Scale should be #{expected_scale} for '#{password}'"
      equal score.value, 0, "Value should be 0 for '#{password}'"

) jQuery
