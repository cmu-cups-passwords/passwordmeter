(($) ->

  rule = 'blacklist'
  widget = 'passwordmeter'

  module "#{widget}: #{rule}"

  test 'exists', ->
    expect 1
    ok rule of $.cups.passwordmeter.rules, 'rule extends widget'

  defaults =
    blacklist: [
      'password',
      'correcthorsebatterystaple'
    ]
  ruleDefaults = $.cups.passwordmeter.rules[rule].prototype.options

  test 'defined defaults', ->
    count = 0
    $.each defaults, (key, val) ->
      expect ++count
      if $.isFunction(val)
        ok $.isFunction(ruleDefaults[key]), key
      else
        deepEqual ruleDefaults[key], val, key

  test 'tested defaults', ->
    count = 0
    $.each ruleDefaults, (key) ->
      expect ++count
      ok key of defaults, key

  test 'rejects password on blacklist', ->
    expect 2
    instance = new $.cups.passwordmeter.rules[rule]()

    equal instance.grade('password', {scale: 1, value: 100}).scale, 0,
      '"password" blacklisted'
    equal instance.grade('correcthorsebatterystaple',
      {scale: 1, value: 100}).scale, 0,
      '"correcthorsebatterystaple" blacklisted'

  test 'rejects password added to blacklist', ->
    expect 2
    instance = new $.cups.passwordmeter.rules[rule]({
      blacklist: [
        'monkey',
        'banana'
      ]
      })

    equal instance.grade('monkey', {scale: 1, value: 100}).scale, 0,
      '"monkey" blacklisted'
    equal instance.grade('password', {scale: 1, value: 100}).scale, 1,
      '"password" no longer blacklisted'
) jQuery
