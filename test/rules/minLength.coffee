(($) ->

  rule = 'minLength'
  widget = 'passwordmeter'

  module "#{widget}: #{rule}"

  test 'exists', ->
    expect 1
    ok rule of $.cups.passwordmeter.rules, 'rule extends widget'

  defaults =
    minLength: 8
  ruleDefaults = $.cups.passwordmeter.rules[rule].prototype.options

  test 'minLength: 8, password: ""', ->
    expect 1
    password = ""
    instance = new $.cups.passwordmeter.rules[rule]()

    equal instance.grade(password, {value: 100, scale: 1}).value, 0

  test 'minLength: 0, password: ""', ->
    expect 1
    password = ""
    instance = new $.cups.passwordmeter.rules[rule]({minLength: 0})

    equal instance.grade(password, {value: 100, scale: 1}).value, 100

  test 'minLength: 8, password: "password"', ->
    expect 1
    password = "password"
    instance = new $.cups.passwordmeter.rules[rule]()

    equal instance.grade(password, {value: 100, scale: 1}).value, 100

  test 'minLength: 10, password: "password"', ->
    expect 1
    password = "password"
    instance = new $.cups.passwordmeter.rules[rule]({minLength: 10})

    equal instance.grade(password, {value: 100, scale: 1}).value, 32

  test 'defined defaults', ->
    count = 0
    $.each defaults, (key, val) ->
      expect ++count
      if $.isFunction(val)
        ok $.isFunction(ruleDefaults[key]), key
      else
        deepEqual ruleDefaults[key], val, key

  test 'tested defaults', ->
    count = 0
    $.each ruleDefaults, (key) ->
      expect ++count
      ok key of defaults, key

) jQuery
