(($) ->

  rule = 'challengeFactor'
  widget = 'passwordmeter'

  module "#{widget}: #{rule}"

  test 'exists', ->
    expect 1
    ok rule of $.cups.passwordmeter.rules, 'rule extends widget'

  defaults =
    challengeFactor: 2.0
  ruleDefaults = $.cups.passwordmeter.rules[rule].prototype.options

  test 'defined defaults', ->
    count = 0
    $.each defaults, (key, val) ->
      expect ++count
      if $.isFunction(val)
        ok $.isFunction(ruleDefaults[key]), key
      else
        deepEqual ruleDefaults[key], val, key

  test 'tested defaults', ->
    count = 0
    $.each ruleDefaults, (key) ->
      expect ++count
      ok key of defaults, key

  test 'scales by default factor', ->
    expect 2

    instance = new $.cups.passwordmeter.rules[rule]()
    equal instance.grade('', {scale: 1, score: 100}).scale, 0.5,
      'empty password'
    equal instance.grade('password', {scale: 1, score: 100}).scale, 0.5,
      'non-empty password'

  test 'scales by custom factor', ->
    expect 2

    instance = new $.cups.passwordmeter.rules[rule]({challengeFactor: 4.0})
    equal instance.grade('', {scale: 1, score: 100}).scale, 0.25,
      'empty password'
    equal instance.grade('password', {scale: 1, score: 100}).scale, 0.25,
      'non-empty password'

) jQuery
